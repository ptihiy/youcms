<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Author::class, 20)->create()->each(function($a) {
            $a->videos()->save(factory(App\Video::class)->make());
        });
    }
}
