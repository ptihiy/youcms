<?php

use Faker\Generator as Faker;

$factory->define(App\Video::class, function (Faker $faker) {
    return [
        'name' => $faker->catchPhrase,
        'summary' => $faker->paragraph,
        'link' => $faker->uuid,
        'url' => $faker->url,
    ];
});
