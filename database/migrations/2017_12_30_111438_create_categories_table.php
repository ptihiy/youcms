<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedSmallInteger('parent_id')
              ->default(0);
            $table->string('name', 255);
            $table->string('url', 255);
            $table->text('summary')
              ->nullable();
            $table->text('content')
              ->nullable();
            $table->unsignedSmallInteger('position')
                ->default(0);
            $table->string('image', 255)
                ->nullable();
            $table->string('meta_title', 255)
                ->nullable();
            $table->string('meta_keywords', 255)
                ->nullable();
            $table->string('meta_description', 255)
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
