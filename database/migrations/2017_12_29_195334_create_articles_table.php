<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name', 255);
          $table->string('url', 255);
          $table->boolean('published')
            ->default(true);
          $table->text('summary')
            ->nullable();
          $table->text('content')
            ->nullable();
          $table->string('image', 255)
              ->nullable();
          $table->unsignedSmallInteger('created_by')
              ->nullable();
          $table->unsignedSmallInteger('modified_by')
                ->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
