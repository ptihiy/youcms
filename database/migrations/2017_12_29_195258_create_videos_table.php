<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('summary')
              ->nullable();
            $table->string('link', 255);
            $table->text('content')
              ->nullable();
            $table->unsignedSmallInteger('category_id')
              ->nullable();
            $table->unsignedInteger('author_id');
            $table->foreign('author_id')
                ->references('id')
                ->on('authors')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
            $table->unsignedMediumInteger('views')
                ->default(0);
            $table->string('url', 255);
            $table->string('link', 255);
            $table->unsignedSmallInteger('created_by')
                ->nullable();
            $table->unsignedSmallInteger('modified_by')
                  ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
