-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: multiki
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `summary` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` smallint(5) unsigned DEFAULT NULL,
  `modified_by` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (1,'Disney','','111','2018-01-19 19:09:32','2018-01-19 19:09:32'),(2,'Одесская киностудия','odesskaya-kinostudiya','ывыв','2018-01-20 11:31:20','2018-03-13 15:37:36'),(3,'Dr. Daphney Gutkowski','http://heathcote.org/saepe-esse-amet-quo-accusantium-pariatur-aliquam-ut','6031f463-acf9-3397-ac94-b7f8f179bc90','2018-03-22 16:28:41','2018-03-22 16:28:41'),(4,'Abelardo Herzog','https://weissnat.net/quisquam-rem-illum-enim-dolorem-esse-blanditiis-tempora.html','463db43a-6c19-3107-af6d-ec36c3b43c92','2018-03-22 16:28:41','2018-03-22 16:28:41'),(5,'Stefan Bashirian','http://www.reilly.com/consectetur-quaerat-consequuntur-reiciendis-sequi-voluptatum-et-velit-at','63849935-106f-3b1d-b0d9-82f9ce87512d','2018-03-22 16:28:41','2018-03-22 16:28:41'),(6,'Prof. Mazie Welch DDS','http://www.thiel.com/aut-pariatur-in-ut-dolorem-voluptatum','b534a4d9-8819-367e-81f7-fa4273443c2c','2018-03-22 16:28:41','2018-03-22 16:28:41'),(7,'Prof. Hermann Lubowitz II','https://green.com/ut-omnis-consequatur-doloremque-aut.html','51c50d0a-48c8-3abf-84d0-67768f438e44','2018-03-22 16:28:41','2018-03-22 16:28:41'),(8,'Tad Robel I','http://yost.org/','f52d2441-115b-37bb-9c15-c591927a5387','2018-03-22 16:28:41','2018-03-22 16:28:41'),(9,'Koby Welch Jr.','http://www.cummerata.com/laboriosam-est-ut-et-et','7d792453-9a31-3d7a-a349-c68068d05fb6','2018-03-22 16:28:41','2018-03-22 16:28:41'),(10,'Jordi Romaguera','http://nienow.com/','38d93fc4-0f00-3b96-9e1e-ffe244c2acb2','2018-03-22 16:28:41','2018-03-22 16:28:41'),(11,'Danial Maggio','http://www.gleichner.com/voluptate-aut-accusantium-voluptatem-molestiae-sint-totam-ut-provident.html','25d4e466-8d8f-38a3-9562-f15589fba562','2018-03-22 16:28:41','2018-03-22 16:28:41'),(12,'Sarah Terry','http://www.dooley.info/voluptate-occaecati-pariatur-et-aspernatur','ee9d3ab4-07c1-315d-af58-b831d1e32fe7','2018-03-22 16:28:41','2018-03-22 16:28:41'),(13,'Genesis Okuneva PhD','http://www.bruen.com/consequatur-nemo-fugiat-eveniet-et-fugiat-qui','63c47ae5-bac8-3f65-bcfd-edea724fab89','2018-03-22 16:28:41','2018-03-22 16:28:41'),(14,'Scot Buckridge','http://gorczany.com/','04d279f0-ebf7-311d-9ff4-61b4831ee12c','2018-03-22 16:28:41','2018-03-22 16:28:41'),(15,'Prof. Kaylin Moore DDS','http://gislason.info/rerum-at-iste-dolores-non-iusto-aut-excepturi-reiciendis','6bc1e77d-dab4-30af-ac3c-98fea871ecd2','2018-03-22 16:28:41','2018-03-22 16:28:41'),(16,'Estefania Swift II','http://schneider.org/tenetur-est-architecto-culpa','1db234e6-8bd1-35bd-85e2-a9a4af5ff7ab','2018-03-22 16:28:41','2018-03-22 16:28:41'),(17,'Donato Jacobi','http://www.rath.net/cupiditate-quia-ut-qui-minima-ratione-repudiandae-sed-ea','8a005084-a97a-3c85-a905-602e3fe29706','2018-03-22 16:28:41','2018-03-22 16:28:41'),(18,'Rylee Brakus','http://halvorson.com/neque-dolorem-voluptates-consectetur.html','54c3ac55-f56e-3b88-8c18-eff12606942d','2018-03-22 16:28:41','2018-03-22 16:28:41'),(19,'Octavia Bergnaum Jr.','http://www.lubowitz.com/voluptatem-et-aspernatur-aliquam-et-dolores-aut-minus','e01141c0-1bfa-333b-9d02-912bdc3ac2a6','2018-03-22 16:28:41','2018-03-22 16:28:41'),(20,'Marlee Wisoky','http://www.barrows.com/veritatis-blanditiis-ex-voluptatem-laboriosam','ca055efb-d9ce-34a7-b497-eaef1ecf7564','2018-03-22 16:28:41','2018-03-22 16:28:41'),(21,'Madisen Gorczany','https://www.cole.com/similique-omnis-voluptates-libero-cum-et-est-reiciendis','57a476de-8bd1-3f9f-b11c-f211fd6e4665','2018-03-22 16:28:41','2018-03-22 16:28:41'),(22,'Mr. Joseph Shields PhD','https://www.monahan.com/repellat-placeat-sit-repellat-omnis-adipisci','cdc287a9-d933-35a9-b02f-39ed525e9054','2018-03-22 16:28:41','2018-03-22 16:28:41'),(23,'Melba Zulauf','http://rippin.com/alias-magnam-quis-iusto-minima-commodi-molestiae-iure.html','11881ba9-9693-37d9-af87-f34ba2396171','2018-03-22 16:35:52','2018-03-22 16:35:52'),(24,'Cynthia Hauck','http://krajcik.org/rerum-numquam-quod-eum-accusantium','29b872b6-8ac1-3a86-9805-ca4e4c2ea1f7','2018-03-22 16:35:52','2018-03-22 16:35:52'),(25,'Prof. Norris Hayes','http://www.kohler.biz/velit-modi-velit-quia-error-ullam-officia.html','5c81b081-c6dd-3fc8-8b33-d2692e3fb636','2018-03-22 16:35:52','2018-03-22 16:35:52'),(26,'Belle Grant','http://www.haag.info/rerum-cupiditate-iusto-esse-aut-blanditiis-repellat-ut','142b39af-a96c-3d5c-ba9c-9cd86c8e6d05','2018-03-22 16:35:52','2018-03-22 16:35:52'),(27,'Alex Davis','https://legros.com/similique-rem-quis-doloribus.html','40ca38e5-8770-34fd-a914-29366f9e999d','2018-03-22 16:35:52','2018-03-22 16:35:52'),(28,'Coleman Beatty','http://www.abbott.com/','1576600a-fd5b-38cd-a61d-084b53d2f70e','2018-03-22 16:35:52','2018-03-22 16:35:52'),(29,'Grayce Johns','http://www.paucek.biz/','931f9cf0-aaf0-3900-9a8f-9f96622a513f','2018-03-22 16:35:52','2018-03-22 16:35:52'),(30,'Prof. Liam Murray III','http://www.klein.info/debitis-reprehenderit-quia-aut','0a7c7d05-01d0-3016-bb30-bf6b65361856','2018-03-22 16:35:52','2018-03-22 16:35:52'),(31,'Dr. Eudora Schaefer','http://www.wiza.info/officia-enim-reprehenderit-provident-alias','b9e62a44-7e88-3771-9cd1-cbedf7bc1e9e','2018-03-22 16:35:52','2018-03-22 16:35:52'),(32,'Gregoria Wolf','http://dietrich.com/et-dolorum-tempora-dolorem-consequuntur-quae-nostrum','0f1151bb-c427-33e0-86bc-970fe82124e1','2018-03-22 16:35:52','2018-03-22 16:35:52'),(33,'Margarete Macejkovic','http://abernathy.org/impedit-velit-quisquam-aut-delectus-quia-ex-quidem','66ee037a-ea19-35a7-97cb-76003bb6b7d2','2018-03-22 16:35:52','2018-03-22 16:35:52'),(34,'Pearline O\'Kon','http://www.schuster.com/omnis-voluptatum-unde-sapiente-ipsa-doloremque','a039aba0-1c32-39b3-9b32-28654eae7984','2018-03-22 16:35:52','2018-03-22 16:35:52'),(35,'Dr. Heath Prohaska Jr.','http://www.pfannerstill.net/ducimus-aut-ut-qui-et-voluptates','6f258fa3-5b44-3b68-8787-89bd3fe2c2b5','2018-03-22 16:35:52','2018-03-22 16:35:52'),(36,'Shayne Nitzsche IV','http://bosco.net/','218a8cd5-dcdd-3c47-9985-9f08fb03074a','2018-03-22 16:35:52','2018-03-22 16:35:52'),(37,'Brandy Kohler MD','http://mueller.info/quod-maxime-magni-doloribus-voluptatem-in.html','e8ffd14c-e597-3289-91d5-e6faa720b0ef','2018-03-22 16:35:52','2018-03-22 16:35:52'),(38,'Derick Ritchie DVM','http://www.botsford.info/architecto-vero-voluptates-est-voluptas-numquam-aliquid-dolores','1cfc951f-d08c-3600-ae00-fa3b7c926817','2018-03-22 16:35:52','2018-03-22 16:35:52'),(39,'Violet Sipes','http://www.walter.com/sint-ipsum-consequatur-facilis-dolorem.html','f9bd723e-c866-351e-9ed6-5e2473f8ee89','2018-03-22 16:35:52','2018-03-22 16:35:52'),(40,'Yasmin Mueller','http://www.greenfelder.com/autem-in-quas-non-non-quaerat-sint','a9e65e14-ad3c-375a-8ccc-d7d0e81c979a','2018-03-22 16:35:52','2018-03-22 16:35:52'),(41,'Cortez Shields','http://www.block.com/sint-dolor-vel-unde-nulla-impedit.html','fe74b899-3bec-3173-b127-f8cc8cb377d6','2018-03-22 16:35:52','2018-03-22 16:35:52'),(42,'Prof. Joshuah Dare III','http://gaylord.com/et-nihil-temporibus-esse-officiis-quia-quisquam-sed','ad5196c2-d0e9-32fb-8d86-e268cda49cce','2018-03-22 16:35:52','2018-03-22 16:35:52');
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (12,0,'Комедия','komediya','<p>&nbsp;</p>','<p>&nbsp;</p>',0,NULL,NULL,NULL,NULL,'2018-01-20 11:25:47','2018-01-20 11:25:47'),(13,0,'Драма','drama','<p>&nbsp;</p>','<p>&nbsp;</p>',0,NULL,NULL,NULL,NULL,'2018-01-20 11:25:53','2018-01-20 11:25:53'),(14,0,'Боевик','boevik','<p>&nbsp;</p>','<p>&nbsp;</p>',0,NULL,NULL,NULL,NULL,'2018-01-20 11:26:01','2018-01-20 11:26:01');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (2,'2014_10_12_100000_create_password_resets_table',1),(30,'2014_10_12_000000_create_users_table',2),(31,'2017_12_29_175616_laratrust_setup_tables',2),(32,'2017_12_29_195258_create_videos_table',2),(33,'2017_12_29_195319_create_news_posts_table',2),(34,'2017_12_29_195334_create_articles_table',2),(35,'2017_12_29_195527_create_pages_table',2),(36,'2017_12_29_195541_create_comments_table',2),(37,'2017_12_29_195605_create_settings_table',2),(38,'2017_12_30_111438_create_categories_table',2),(39,'2018_01_19_192843_create_tags_table',3),(40,'2018_01_19_215737_create_authors_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_posts`
--

DROP TABLE IF EXISTS `news_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `summary` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` smallint(5) unsigned DEFAULT NULL,
  `modified_by` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_posts`
--

LOCK TABLES `news_posts` WRITE;
/*!40000 ALTER TABLE `news_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `summary` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` smallint(5) unsigned DEFAULT NULL,
  `modified_by` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user`
--

DROP TABLE IF EXISTS `permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user` (
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  KEY `permission_user_permission_id_foreign` (`permission_id`),
  CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user`
--

LOCK TABLES `permission_user` WRITE;
/*!40000 ALTER TABLE `permission_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'manager','manager',NULL,NULL,NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (1,1,'App\\User');
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'manager','manager',NULL,NULL,NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` tinyint(3) unsigned DEFAULT NULL,
  `modified_by` tinyint(3) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Pavel','ptihiy@gmail.com','$2y$10$59UbGUtFwfvuEdpDAAKVzek/GLm7Gvl80YQYLleys0FS/idopQCFS',NULL,'2018-03-13 16:31:57','2018-03-13 16:31:57');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `author_id` int(10) unsigned DEFAULT NULL,
  `views` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` smallint(5) unsigned DEFAULT NULL,
  `modified_by` smallint(5) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  CONSTRAINT `videos_ibfk_2` FOREIGN KEY (`author_id`) REFERENCES `authors` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (2,'Хочу вашего мужа',NULL,'FQRqJgKLZDw',NULL,12,2,0,'hochu-vashego-muzha',NULL,NULL,'2018-01-20 11:31:41','2018-03-13 15:35:07'),(3,'Enhanced full-range methodology','Quis ut qui eveniet quidem autem. Quas in blanditiis non aut. Totam qui quae quod voluptatem et voluptatem earum.','8b5b57c8-66a8-37d8-b5b4-1263f11ad861',NULL,NULL,23,0,'http://www.larson.com/distinctio-sed-numquam-eligendi-reprehenderit-quisquam.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(4,'User-centric 24/7 strategy','Veniam enim et porro facere iste debitis hic. Laudantium est voluptatem necessitatibus quia qui ipsam. Enim veritatis sunt ut porro molestiae suscipit non nostrum. Quod minus error ratione. Praesentium omnis ut iure amet ducimus dolorem sed pariatur.','e5e6b2e4-7799-3531-8a86-29b96aeeeeec',NULL,NULL,24,0,'http://www.okeefe.com/ab-sed-ipsam-blanditiis-occaecati-eos-quasi.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(5,'Multi-lateral impactful archive','Rerum mollitia nemo libero. Aut distinctio eius ea.','674614bd-6161-3817-aa2c-591a1b0263a5',NULL,NULL,25,0,'http://www.maggio.info/quis-ullam-sed-ipsam-velit',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(6,'Right-sized mobile protocol','Nihil ullam magnam aut dolorem. Eos omnis sapiente neque. Nisi aut eligendi est a voluptatum. Enim iste consectetur voluptas neque qui dolorum.','4cdb488a-a77f-353b-9bd3-93aab604f3b3',NULL,NULL,26,0,'http://www.goyette.biz/doloremque-nisi-itaque-ut-blanditiis.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(7,'Robust bi-directional encoding','Maiores autem sunt iure officiis aut. Error quia ut eum eveniet qui necessitatibus voluptas. Repellendus accusantium velit laboriosam voluptate perspiciatis perspiciatis soluta voluptate. Modi maxime vel incidunt omnis eum id.','8dfc4769-e088-3617-adbe-5d4290125f1c',NULL,NULL,27,0,'http://www.barrows.com/dolore-rerum-aut-repudiandae-rerum.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(8,'Synchronised zeroadministration ability','Et ad cum fugit fugiat. A eum occaecati et dolor temporibus et aut. Dolores est in quasi minima magnam sapiente enim. Minima inventore soluta et voluptas saepe alias.','7f8744ef-79ae-3df9-9589-81fc91d96379',NULL,NULL,28,0,'http://runte.net/quia-facere-autem-quis.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(9,'Function-based 5thgeneration capacity','Ea dolor sint qui necessitatibus. Nam quo iure magni quaerat rerum.','88670f6e-830b-3b3d-8574-0c4baa8a3039',NULL,NULL,29,0,'http://morissette.info/aspernatur-omnis-magnam-assumenda-et-quidem',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(10,'Cross-platform 4thgeneration systemengine','Sit odit minima qui rem. Non et explicabo veritatis voluptatem quod. Unde dolores facere voluptates.','3a3b5c5e-4623-3a17-9dba-efb0de07cd48',NULL,NULL,30,0,'http://www.schaden.com/',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(11,'Pre-emptive tertiary GraphicInterface','Sed cumque quis quas harum. Et ipsum natus commodi veniam aut omnis quibusdam assumenda. Repellendus molestiae unde eos fugiat rerum. Cupiditate dolor aliquam sed necessitatibus.','d5825992-3af7-3702-bda6-b92ae7ffa5fb',NULL,NULL,31,0,'http://hand.com/voluptas-illum-qui-ducimus-voluptas.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(12,'Seamless cohesive methodology','Ad et et accusantium in. Minima esse rerum distinctio.','484ea763-f884-31ad-9506-a97f209c43f9',NULL,NULL,32,0,'http://considine.org/qui-eos-deleniti-sunt-quis-sunt-dolorem-cum.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(13,'Self-enabling value-added model','Maiores unde consectetur voluptatibus quia. Mollitia similique voluptate quia magni. Dolores est alias eos totam.','f4122c3a-f11d-3144-acc8-5e77cd4172ec',NULL,NULL,33,0,'http://www.schumm.com/nisi-debitis-quis-et-numquam-expedita-aliquid',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(14,'Down-sized content-based firmware','Dolore sed quaerat qui nihil aliquid magnam sit. Qui voluptatem aliquam est dolor. Fugit nulla quidem molestiae veritatis quia facere. Eius voluptatem est recusandae adipisci ipsa.','69e93044-aca5-333b-97cd-2c49e79dd671',NULL,NULL,34,0,'https://funk.info/facilis-nisi-aperiam-asperiores-dolores-a.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(15,'Advanced secondary openarchitecture','Sit illo dicta iusto in et. Fugiat a consequatur quaerat velit. Repellat sint reiciendis et nulla magni.','12b8c15b-8f81-36b3-b68b-c06a1b11efdf',NULL,NULL,35,0,'http://www.schultz.com/',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(16,'Adaptive high-level hardware','Mollitia suscipit consequuntur reprehenderit aut. Omnis inventore et tenetur beatae consequatur. Aut quia iusto quo voluptas quis et pariatur nulla. At qui ut atque voluptate velit.','2e3498de-aac5-3f3e-8989-a60edc6ce2f1',NULL,NULL,36,0,'http://doyle.com/',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(17,'Vision-oriented asynchronous solution','Rerum exercitationem vel quia quo. Voluptas quis debitis necessitatibus officia nihil mollitia. Rerum voluptatem maiores vitae fuga eum officia praesentium et.','86906536-49e0-3eb1-b488-85ce281317c6',NULL,NULL,37,0,'http://www.pfeffer.info/fuga-beatae-quia-laudantium-consequuntur-sunt-qui.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(18,'Ergonomic grid-enabled installation','Eos iusto id consequatur consectetur non. Quia molestiae vero provident voluptas ut. Omnis eum modi porro ea nihil ut sit. Tempore recusandae et qui voluptatem.','96b65de4-1953-3664-9e1b-cb0e20937582',NULL,NULL,38,0,'http://oconner.com/',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(19,'Intuitive high-level matrix','Doloribus accusamus quibusdam inventore a dolores exercitationem. Laudantium perspiciatis fugit rerum similique laborum qui nulla. Ea labore eveniet dolorem facere ut dicta.','fb13572b-d7a8-36cf-8e00-6a6f4e68c681',NULL,NULL,39,0,'http://www.waters.biz/atque-omnis-amet-qui',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(20,'Reactive didactic architecture','Consequuntur sed et voluptatem recusandae vitae molestias incidunt. Aliquam sit sequi unde at. Ipsa consectetur repudiandae quod eligendi eum sunt.','b3cd8e4c-4e02-3b76-b116-3b67908a3b65',NULL,NULL,40,0,'https://www.skiles.net/hic-exercitationem-in-voluptas-officia',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(21,'Adaptive 5thgeneration product','Aut tempora ullam asperiores. Quidem id et sed voluptatem reiciendis voluptatibus est. Eaque eum quasi consequatur vel exercitationem.','8eca4f90-5442-3390-8381-16f44cdc4998',NULL,NULL,41,0,'http://www.wintheiser.com/rerum-earum-aliquam-alias-quo-aut.html',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52'),(22,'Digitized executive interface','Delectus quia nemo voluptatem mollitia ipsam officia. Eum quam quis blanditiis minus consequatur reprehenderit sapiente. Mollitia ducimus voluptas consequatur dolor hic voluptatem tempora quam. Rerum officiis aut odio est velit debitis omnis possimus.','5f12a5c9-0b8b-3cfc-aec3-295c18c5db7b',NULL,NULL,42,0,'http://leannon.com/quam-sed-distinctio-quibusdam',NULL,NULL,'2018-03-22 16:35:52','2018-03-22 16:35:52');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-17 17:08:52
