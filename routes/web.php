<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('backend')
  ->namespace('Backend')
  ->prefix('manager')
  ->as('backend.')
  ->group(function() {
    Route::get('/', 'DashboardController@index')
      ->name('dashboard');

    Route::post('categories/move', 'CategoriesController@move')
          ->name('categories.move');
    Route::resource('/categories', 'CategoriesController');

    Route::get('/videos/request', 'VideosController@request')
        ->name('videos.request');
    Route::post('/videos/import', 'VideosController@import')
        ->name('videos.import');
    Route::resource('/videos', 'VideosController');

      Route::get('/authors/request', 'AuthorsController@request')
          ->name('authors.request');
      Route::post('/authors/import', 'AuthorsController@import')
          ->name('authors.import');
    Route::resource('/authors', 'AuthorsController');

    Route::resource('/news', 'NewsController');

    Route::resource('/articles', 'ArticlesController');

    Route::resource('/pages', 'PagesController');

    Route::resource('/comments', 'CommentsController');

    Route::resource('/settings', 'SettingsController');

    Route::resource('/permissions', 'PermissionsController');

    Route::resource('/roles', 'RolesController');

    Route::resource('/managers', 'ManagersController');
  });

Auth::routes();

Route::middleware('frontend')
    ->namespace('Frontend')
    ->group(function() {
        Route::get('/', 'IndexController@index');

        Route::get('/articles/{url}', 'ArticlesController@show');

        Route::get('/categories/{url}', 'CategoriesController@show');

        Route::get('/authors/{url}', 'AuthorsController@show');

        Route::get('/{url}', 'VideosController@show');

    });
