<h4 class="text-center">Новые фильмы</h4>
<div>
    @foreach ($lastVideos as $lv)
        <article class="promo-video-thumb clearfix">
            <div class="pic">
                <a href="#">
                    <img src="/images/videos/{{ $lv->url }}_default.jpg" alt="{{ $lv->name }}" class="cast img-responsive">
                </a>
                <span class="duration"><i class="far fa-clock"></i> {{ duration2view($lv->duration) }}</span></a>
            </div>
            <div class="description">
                <h5>{{ str_limit($lv->name, 50) }}</h5>
                <a class="author" href="/authors/{{ $lv->author->url }}">
                    <i class="fas fa-video"></i> {{ $lv->author->name }}
                </a>
            </div>
        </article>
    @endforeach
</div>
<div class="text-center">
    <a class="promotion-main-link" href="/videos">
        Все видео
    </a>
</div>
