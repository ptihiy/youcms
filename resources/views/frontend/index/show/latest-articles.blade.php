<h4 class="text-center">Новые статьи</h4>
<div>
    <article class="promo-article-thumb clearfix">
        <div class="pic">
            <a href="#">
                <img src="http://via.placeholder.com/120x90">
            </a>
        </div>
        <div class="description">
            <h5>Название статьи как единственная сущность бытия</h5>
            <div class="summary">
                <p>Я едва представлял, когда шел по залитому золотым солнцем лугу, что мой конь доедает подпругу.</p>
            </div>
        </div>
    </article>
    <article class="promo-article-thumb clearfix">
        <div class="pic">
            <a href="#">
                <img src="http://via.placeholder.com/120x90">
            </a>
        </div>
        <div class="description">
            <h5>Название статьи как единственная сущность бытия</h5>
            <div class="summary">
                <p>Я едва представлял, когда шел по залитому золотым солнцем лугу, что мой конь доедает подпругу.</p>
            </div>
        </div>
    </article>
    <article class="promo-article-thumb clearfix">
        <div class="pic">
            <a href="#">
                <img src="http://via.placeholder.com/120x90">
            </a>
        </div>
        <div class="description">
            <h5>Название статьи как единственная сущность бытия</h5>
            <div class="summary">
                <p>Я едва представлял, когда шел по залитому золотым солнцем лугу, что мой конь доедает подпругу.</p>
            </div>
        </div>
    </article>
    <article class="promo-article-thumb clearfix">
        <div class="pic">
            <a href="#">
                <img src="http://via.placeholder.com/120x90">
            </a>
        </div>
        <div class="description">
            <h5>Название статьи как единственная сущность бытия</h5>
            <div class="summary">
                <p>Я едва представлял, когда шел по залитому золотым солнцем лугу, что мой конь доедает подпругу.</p>
            </div>
        </div>
    </article>
</div>
<div class="text-center">
    <a class="promotion-main-link" href="/articles">
        Все статьи
    </a>
</div>
