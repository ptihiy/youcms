<div>
    <div class="widget">
        <h4>Топ популярных</h4>
        @foreach($popularVideos as $pv)
        <div class="video-thumb clearfix">
            <div class="screencast float-left">
                <a href="/{{ $pv->url }}">
                    <img src="/images/videos/{{ $pv->url }}_default.jpg" alt="{{ $pv->name }}">
                    <span class="duration"><i class="far fa-clock"></i> {{ duration2view($pv->duration) }}</span></a>
            </div>
            <div class="media-body">
                <a href="/{{ $pv->url }}">
                    <h6>{{ str_limit($pv->name, 50) }}</h6>
                </a>
                <a class="author" href="/authors/{{ $pv->author->url }}">
                    <i class="fas fa-video"></i> {{ $pv->author->name }}
                </a>
            </div>
        </div>
        @endforeach
    </div>
</div>