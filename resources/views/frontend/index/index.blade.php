@extends('frontend.master')

@section('main')

    <section class="row search_filter search_filter_type2 retro-ribbon">
        <div class="container">
            <div class="row m0">
            </div>
        </div>
    </section> <!--Search Filter-->

    <section class="row post_page_sidebar post_page_sidebar1">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 post_page_uploads">
                    <div class="promotions">
                        <div class="row">
                            <div class="col-md-6">
                                @include('frontend.index.show.latest-videos')
                            </div>
                            <div class="col-md-6">
                                @include('frontend.index.show.latest-articles')
                            </div>
                        </div>
                    </div>
                    <div class="row pagination_row">
                        <ul class="pagination">
                            <li><a href="#">previous</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li class="disabled"><a href="#">...</a></li>
                            <li><a href="#">next</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 sidebar sidebar2">
                    <div class="row m0 sidebar_row_inner">
                        <!--Popular Videos-->
                        @include('frontend.index.show.popular-videos')
                        <!--Recommended to Follow-->
                        @include('frontend.index.show.followed-authors')
                    </div>
                </div>
            </div>
        </div>
    </section> <!--Uploads-->
@endsection
