@extends('frontend.master')

@section('main')
    @include('frontend.authors.show.breadcrumbs')
    @include('frontend.partials.ribbon')
    @include('frontend.authors.show.videos')
    @include('frontend.authors.show.advertise')
    @include('frontend.authors.show.aside')
    @include('frontend.partials.ribbon')
@endsection
