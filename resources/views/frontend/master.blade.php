<!DOCTYPE html>
<html lang="en">
<head>
  @include('frontend.partials.head')
</head>
<body class="home">

    @include('frontend.partials.header')

    @yield('main')

    <section class="row">
        <ul class="nav nav-justified ribbon">
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
        </ul>
    </section> <!--Ribbon-->

    <footer class="row">
    @include('frontend.partials.footer')
    </footer>

	<!-- Preloader -->
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>

    @include('frontend.partials.scripts')
</body>
</html>
