<div class="container">
    <header class="blog-header py-1">
        <div class="row">
            <div class="col-md-6 text-left">
                <a class="blog-header-logo" href="/">
                    Русский фильм
                </a>
            </div>
            <div class="col-md-6 justify-content-end">
                <form action="#" role="search" class="search-form">
                    <div class="input-group">
                        <input type="text" id="search" class="form-control" placeholder="Искать" >
                    </div>
                </form>
            </div>
        </div>
    </header>
    <div class="py-1 mb-1">
        <ul class="nav d-flex main-menu">
            <li><a class="p-2" href="#">Категории</a></li>
            <li><a class="p-2" href="#">Киностудии</a></li>
            <li><a class="p-2" href="#">Актеры</a></li>
            <li><a class="p-2" href="#">Подборки</a></li>
            <li><a class="p-2" href="#">Статьи</a></li>
            <li><a class="p-2" href="#">Новости</a></li>
            <li><a class="p-2" href="#">Контакты</a></li>
        </ul>
    </div>
</div>