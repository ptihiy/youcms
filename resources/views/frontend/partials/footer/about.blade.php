<h4 class="widget_title">О портале</h4>
<div class="row m0 inner">
    <a href="/"><img src="images/footer-logo.png" alt=""></a><br>
    <p>Портал <strong>Старое кино</strong> - удобный сайт для просмотра фильмов с Youtube с удобной навигацией по жанрам, киностудиям, артистам, году выпуска и так далее.</p>
</div>