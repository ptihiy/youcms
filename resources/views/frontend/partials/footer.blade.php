<div class="container">
    <div class="row sidebar sidebar_footer">
        <div class="col-sm-3 widget widget1 w_in_footer widget_about">
            @include('frontend.partials.footer.about')
        </div>
        <div class="col-sm-3 widget widget2 w_in_footer widget_subscribe">
            @include('frontend.partials.footer.subscribe')
        </div>
        <div class="col-sm-3 widget widget3 w_in_footer widget_tags">
            <h4 class="widget_title">Популярные жанры</h4>
            <div class="row m0 inner">
                <a href="#" class="tag">business</a>
                <a href="#" class="tag">osx</a>
                <a href="#" class="tag">windows 10</a>
                <a href="#" class="tag">osx yosemite</a>
                <a href="#" class="tag">photoshop</a>
                <a href="#" class="tag">css</a>
                <a href="#" class="tag">business</a>
                <a href="#" class="tag">osx</a>
                <a href="#" class="tag">windows 10</a>
                <a href="#" class="tag">osx yosemite</a>
                <a href="#" class="tag">photoshop</a>
                <a href="#" class="tag">css</a>
            </div>
        </div>
        <div class="col-sm-3 widget widget4 w_in_footer widget_twitter">
            <h4 class="widget_title">Мы в Twitter</h4>
            <div class="row m0 inner">
                <div class="row m0 tweet"><a href="#">@masum_rana:</a>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                <div class="row m0 tweet"><a href="#">@masum_rana:</a>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
                <div class="row m0 tweet"><a href="#">@masum_rana:</a>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat</div>
            </div>
        </div>
    </div>
</div>
