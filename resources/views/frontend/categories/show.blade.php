@extends('frontend.master')

@section('main')
    @include('frontend.categories.show.breadcrumbs')
    @include('frontend.partials.ribbon')
    @include('frontend.categories.show.videos')
    @include('frontend.categories.show.advertise')
    @include('frontend.categories.show.aside')
    @include('frontend.partials.ribbon')
@endsection
