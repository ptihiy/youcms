<section class="row page_cover">
    <div class="container">
        <div class="row m0">
            <h1><strong>{{ $category->name }}</strong></h1>
            <ol class="breadcrumb">
                <li><a href="/">Главная</a></li>
                <li class="active">{{ $category->name }}</li>
            </ol>
        </div>
    </div>
</section>