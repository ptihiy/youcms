<section class="row recent_uploads">
    <div class="container">
        <div class="row title_row">
            <h3>{{ $videos->total() }} {{ pluralize($videos->total(),'видео,видео,видео') }}</h3>
        </div>
        <div class="row media-grid content_video_posts"><div class="jscroll-inner">
                @foreach ($videos as $video)
                <article class="col-sm-3 video_post postType3">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/1.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">{{ $video->name }}</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">{{ $video->author->name }}</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Art</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                @endforeach
                <article class="col-sm-3 video_post postType4">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/2.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Philosophy</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType2">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/3.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Comedy</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <div class="col-sm-3 video_post advertise_betweeen_uploads">
                    <div class="inner row m0">
                        <h3>Advertise<br>Here</h3>
                    </div>
                </div>
                <article class="col-sm-3 video_post postType3">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/4.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Science</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType4">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/5.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Youtube</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType2">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/6.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">People</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType3">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/7.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Space Tech</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType4">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/8.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Nature &amp; Beauty</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType2">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/9.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Entertainment</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType3">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/10.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Relationship</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <article class="col-sm-3 video_post postType4">
                    <div class="inner row m0">
                        <a href="single-video.html"><div class="row screencast m0">
                                <img src="http://uxart.io.dimgx.net/Products/video-cafe/images/media/11.jpg" alt="" class="cast img-responsive">
                                <div class="play_btn"></div>
                                <div class="media-length">17:30</div>
                            </div></a>
                        <div class="row m0 post_data">
                            <div class="row m0"><a href="single-video.html" class="post_title">Duis aute irure dolor in adipsicing elit</a></div>
                            <div class="row m0">
                                <div class="fleft author">by <a href="page-author.html">Masum Rana</a></div>
                                <div class="fleft date">3 August, 2015</div>
                            </div>
                        </div>
                        <div class="row m0 taxonomy">
                            <div class="fleft category"><a href="category.html"><img src="images/icons/cat.png" alt="">Business</a></div>
                            <div class="fright views"><a href="#"><img src="images/icons/views.png" alt="">3,45,346</a></div>
                        </div>
                    </div>
                </article>
                <div class="row m0">
                    <div class="clearfix"></div>
                    <a href="inc/more-uploads-1.html" class="load_more_videos">Load more videos</a>
                </div>
            </div></div>
    </div>
</section>