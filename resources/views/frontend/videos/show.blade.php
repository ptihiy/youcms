@extends('frontend.master')

@section('main')
    <section class="row page_cover">
        <div class="container">
            <div class="row m0">
                <h1>{{ $video->name }}</h1>
                <ol class="breadcrumb">
                    <li><a href="#">Главная</a></li>
                    <li class="active">{{ $video->name }}</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="row">
        <ul class="nav nav-justified ribbon">
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
        </ul>
    </section>

    <section class="row post_page_sidebar post_page_sidebar1">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 post_page_uploads">
                    <div class="author_details post_details row m0">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{ $video->link }}"></iframe>
                        </div>
                        <div class="row post_title_n_view">
                            <h2 class="col-sm-8 post_title">{{ $video->name }}</h2>
                            <h2 class="col-sm-4 view_count">{{ $video->views }} <small>{{ pluralize($video->views, "просмотр,просмотра,просмотров") }}</small></h2>
                        </div>
                        <div class="media bio_section">
                            <div class="media-left about_social">
                                <div class="row m0 section_row author_section widget widget_recommended_to_follow">
                                    <div class="media">
                                        <div class="media-left"><a href="page-author.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/follow/1.jpg" alt="" class="circle"></a></div>
                                        <div class="media-body media-middle">
                                            <a href="/authors/{{ $video->author->url }}"><h5>{{ $video->author->name }}</h5></a>
                                            <div class="btn-group">
                                                <a href="#" class="btn follower_count">34</a>
                                                <a href="#" class="btn follow">Подписка</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m0 social_section section_row">
                                    <h5>Social Accounts</h5>
                                    <ul class="list-inline">
                                        <li><a href="#"><img src="images/icons/social/1.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/2.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/3.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/4.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/5.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/6.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/7.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/8.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/9.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/10.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/11.jpg" alt=""></a></li>
                                        <li><a href="#"><img src="images/icons/social/12.jpg" alt=""></a></li>
                                    </ul>
                                </div>
                                <div class="row m0 about_section section_row single_video_info">
                                    <dl class="dl-horizontal">
                                        <dt>Publish Date:</dt>
                                        <dd>August 23, 2015</dd>

                                        <dt>Category:</dt>
                                        <dd>Science &amp; Technology</dd>

                                        <dt>Video License</dt>
                                        <dd>Standard License</dd>

                                        <dt>Imported From:</dt>
                                        <dd>Youtube</dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="media-body author_desc_by_author">
                                {!! $video->summary !!}
                            </div>
                        </div>
                    </div>
                    @include('frontend.videos.show.comments')
                </div>
                <div class="col-sm-3 sidebar sidebar2">
                    <div class="row m0 sidebar_row_inner">
                        <!--Search Form-->
                        @include('frontend.partials.search')
                        <!--More From the Author-->
                        <div class="row m0 widget widget_popular_videos">
                            <h4 class="widget_title">Другие видео киностудии</h4>
                            <div class="row m0 inner">
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/1.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/2.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/3.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/4.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Recommended for You-->
                        <div class="row m0 widget widget_popular_videos">
                            <h5 class="widget_title">Recommended for you</h5>
                            <div class="row m0 inner">
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/1.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/2.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/3.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/4.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/1.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/2.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/3.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/4.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/1.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/2.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/3.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                                <div class="media">
                                    <div class="media-left"><a href="single-video.html"><img src="http://uxart.io.dimgx.net/Products/video-cafe/images/popular/4.jpg" alt=""><span class="duration">17:30</span></a></div>
                                    <div class="media-body">
                                        <a href="single-video.html">
                                            <h5>Lorem ipsum dolor si amet etur adipis</h5>
                                        </a>
                                        <div class="row m0 meta_info views">34,000 views</div>
                                        <div class="row m0 meta_info posted">1 year ago</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
