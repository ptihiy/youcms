@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $count.' '.pluralize($count,'категория,категории,категорий')])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.categories.create') }}" class="btn btn-md btn-primary">Создать категорию</a>
      </div>
      <div class="panel-body">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название</th>
                <th>Url</th>
                <th>Операции</th>
            </tr>
            </thead>
        </table>
          <ul id="sortable" class="ul-sortable sortable">
              @include('backend.categories.index.categories', ['categories' => $categories])
          </ul>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        $('.delete').click(function() {
            var $this = $(this);
            var id = $this.closest('tr').data('id');
            $.post('{{ route('backend.categories.index') }}/' + id, { _method: 'delete' }, function() {
                $this.closest('tr').remove();
            });
            return false;
        });
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $('.delete').click(function() {
            var $this = $(this);
            var id = $this.closest('li').data('id');
            $.post('{{ route('backend.categories.index') }}/' + id, { _method: 'delete' }, function() {
                $this.closest('li').remove();
            });
            return false;
        });

        $(".sortable").sortable({
            handle: ".cms-move",
            axis: "y",
            forceHelperSize: true,
            placeholder: "ui-state-highlight",
            connectWith: '.sortable',
            helper: function(e, el) {
                var children = el.children();
                var clone = el.clone();

                clone.children().width(function (i, val) {
                    return children.eq(i).width();
                });

                return clone;
            },
            start: function(e, ui) {
                ui.indexStart = ui.item.index();
            },
            stop: function(e, ui) {
                var $this = $(ui.item);
                var id = $this.data('id');

                // Data to send
                var parent_id = 0;
                var parent = $this.parents('li').eq(0);
                if (parent.length) {
                    parent_id = parent.data('id');
                }

                // Get all li of inner ul with their positions
                var ids = $this.closest('ul').children().map(function() {
                    return $(this).data('id');
                }).toArray();

                var data = {
                    id: id,
                    parent_id: parent_id,
                    ids: ids
                };

                $.post('{{ route('backend.categories.move') }}', data, function() { });
            }
        });
    } );
</script>
<style>
    .ui-state-highlight {
        background: #74cac3;
        border: #74cac3;
    }
</style>
@endpush
