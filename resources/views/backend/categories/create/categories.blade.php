@foreach ($categories as $c)
    <option value="{{ $c['url'] }}">{{ str_repeat("&nbsp;", 4 * $c['level']) }}{{ $c['name']
    }}</option>
    @if (!empty($c['children']))
        @include('backend.categories.create.categories', ['categories' => $c['children']])
    @endif
@endforeach