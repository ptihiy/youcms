@foreach($categories as $category)
    <li data-id="{{ $category['id'] }}" data-position="{{ $category['position'] }}">
        <div class="faux-row clearfix">
            <div><i class="fa fa-bars cms-move" aria-hidden="true"></i></div>
            <div class="name">
                <a href="{{ route('backend.categories.edit', $category['id']) }}">
                    {{ $category['name']  }}
                </a>
            </div>
            <div>{{ $category['url'] }}</div>
            <div class="td-operations">
                <div class="operations">
                    <a href="#" class="delete">
                        <i class="fa fa-remove" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        @if (!empty($category['children']))
            <ul class="sortable">
                @include('backend.categories.index.categories', ['categories' => $category['children']])
            </ul>
        @else
            <ul class="sortable"></ul>
        @endif
    </li>
@endforeach