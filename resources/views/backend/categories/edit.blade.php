@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $category->name])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.categories.update', $category->id) }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" name="name" id="name"
                   data-transliterable="url" value="{{ $category->name }}">
          </div>
          <div class="form-group">
            <label for="value">Url</label>
            <input type="text" class="form-control" name="url" id="url" value="{{ $category->url }}">
          </div>
            <div class="form-group">
                <label for="parent_id">Родетельская категория</label>
                <select name="parent_id" id="parent_id" class="form-control">
                    <option value="">нет</option>
                    @if (!empty($categories))
                        @include('backend.categories.create.categories', ['categories' => $categories])
                    @endif
                </select>
            </div>
          <div class="form-group">
            <label for="summary">Краткий текст</label>
            <textarea class="form-control ckeditored" id="summary" name="summary" rows="3">{{ $category->summary }}</textarea>
          </div>
          <div class="form-group">
            <label for="content">Текст</label>
            <textarea class="form-control ckeditored" id="content" name="content" rows="3">{{ $category->content }}</textarea>
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Обновить</button>
        </form>
    </div>
  </div>
</div>
</div>
@endsection
@push('scripts')
<script src="/js/ckeditor.js"></script>
<script>
ClassicEditor
    .create( document.querySelector( '#summary' ) )
    .then( editor => {
        window.editor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
ClassicEditor
    .create( document.querySelector( '#content' ) )
    .then( editor => {
        window.editor = editor;
    } )
    .catch( err => {
        console.error( err.stack );
    } );
</script>
@endpush
