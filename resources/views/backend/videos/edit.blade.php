@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $video->name])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.videos.update', $video->id) }}">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="form-group">
            <label for="name">Название</label>
            <input type="text" data-transliterable="url" class="form-control" name="name" id="name" value="{{ $video->name }}">
          </div>
          <div class="form-group">
            <label for="url">Url</label>
            <input type="text" class="form-control" name="url" id="url" value="{{ $video->url }}">
          </div>
          <div class="form-group">
            <label for="author_id">Автор</label>
            <select class="form-control" name="author_id" id="author_id">
              @foreach ($authors as $author)
                <option value="{{ $author->id }}"
                        {{ $video->author_id == $author->id ? 'selected' : '' }}
                >{{ $author->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="url">Категория</label>
            <select class="form-control" name="category_id" id="category_id">
              @foreach ($categories as $category)
                <option value="{{ $category->id }}"
                  {{ $video->category_id == $category->id ? 'selected' : '' }}
                >{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="link">Ссылка Youtube</label>
            <input type="text" class="form-control" name="link" id="link" value="{{ $video->link }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Обновить</button>
        </form>
    </div>
  </div>
</div>
@endsection
