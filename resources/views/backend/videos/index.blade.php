@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $videos->total().' '.pluralize($videos->total(), 'видео,видео,видео')])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.videos.create') }}" class="btn btn-md btn-primary">Создать видео</a>
        <a href="{{ route('backend.videos.request') }}" class="btn btn-md btn-primary">Импортировать видео</a>
      </div>
        <div class="panel-body">
          {{ $videos->links() }}
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Название</th>
              <th>Url</th>
              <th>Автор</th>
              <th>Операции</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($videos as $video)
              <tr>
                <td><a href="{{ route('backend.videos.edit', $video->id) }}">{{ $video->name }}</a></td>
                <td>{{ $video->url }}</td>
                <td>{{ !empty($video->author) ? $video->author->name : "" }}</td>
                <td></td>
              </tr>
            @empty
              <tr>
                <td colspan="3">Видео не добавлены</td>
              </tr>
            @endforelse
            </tbody>
          </table>
          {{ $videos->links() }}
      </div>
  </div>
</div>
@endsection
