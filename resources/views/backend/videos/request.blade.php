@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Импорт видео'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.videos.import') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="link">Ссылка Youtube</label>
            <input type="text" class="form-control" name="link" id="link" value="{{ old('link') }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Создать</button>
          <button type="submit" name="more" value="1" class="btn btn-default">Создать и добавить еще</button>
        </form>
    </div>
  </div>
</div>
@endsection
