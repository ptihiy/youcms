@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Новое видео'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.videos.store') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Название</label>
            <input type="text" data-transliterable="url" class="form-control" name="name" id="name" value="{{ old('name') }}">
          </div>
          <div class="form-group">
            <label for="url">Url</label>
            <input type="text" class="form-control" name="url" id="url" value="{{ old('url') }}">
          </div>
          <div class="form-group">
            <label for="author_id">Автор</label>
            <select class="form-control" name="author_id" id="author_id">
              @foreach ($authors as $author)
                <option value="{{ $author->id }}">{{ $author->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="category_id">Категория</label>
            <select class="form-control" name="category_id" id="category_id">
              @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>
          <div class="form-group">
            <label for="link">Ссылка Youtube</label>
            <input type="text" class="form-control" name="link" id="link" value="{{ old('link') }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Создать</button>
          <button type="submit" name="more" value="1" class="btn btn-default">Создать и добавить еще</button>
        </form>
    </div>
  </div>
</div>
@endsection
