@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Новая настройка'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.settings.store') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
          </div>
          <div class="form-group">
            <label for="value">Значение</label>
            <input type="text" class="form-control" name="value" id="value" value="{{ old('name') }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Создать</button>
          <button type="submit" name="more" value="1" class="btn btn-default">Создать и добавить еще</button>
        </form>
    </div>
  </div>
</div>
@endsection
