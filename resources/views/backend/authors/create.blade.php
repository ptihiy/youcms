@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Новый автор'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.authors.store') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control" data-transliterable="url"  name="name" id="name" value="{{ old('name') }}">
          </div>
          <div class="form-group">
            <label for="channel_id">Id канала</label>
            <input type="text" class="form-control" name="channel_id" id="channel_id" value="{{ old('channel_id') }}">
          </div>
          <div class="form-group">
            <label for="url">Url</label>
            <input type="text" class="form-control" name="url" id="url" value="{{ old('url') }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Создать</button>
          <button type="submit" name="more" value="1" class="btn btn-default">Создать и добавить еще</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
