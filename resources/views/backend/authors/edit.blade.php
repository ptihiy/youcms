@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $author->name])

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-body">
          <form method="POST" action="{{ route('backend.authors.update', $author->id) }}">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
              <label for="name">Название</label>
              <input type="text" class="form-control" data-transliterable="url"  name="name" id="name" value="{{ $author->name }}">
            </div>
            <div class="form-group">
              <label for="channel_id">Id канала</label>
              <input type="text" class="form-control" name="channel_id" id="hannel_id" value="{{ $author->channel_id }}">
            </div>
            <div class="form-group">
              <label for="url">Url</label>
              <input type="text" class="form-control" name="url" id="url" value="{{ $author->url }}">
            </div>
            <button type="submit" name="save" value="1" class="btn btn-default">Обновить</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection