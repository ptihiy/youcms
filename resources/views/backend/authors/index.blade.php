@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $authors->total().' '.pluralize($authors->total(), 'автор,автора,авторов')])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.authors.create') }}" class="btn btn-md btn-primary">Создать автора</a>
        <a href="{{ route('backend.authors.request') }}" class="btn btn-md btn-primary">Импортировать автора</a>
      </div>
        <div class="panel-body">
          {{ $authors->links() }}
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Название</th>
              <th>Url</th>
              <th>Id канала</th>
              <th class="operations">Операции</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($authors as $author)
              <tr data-id="{{ $author->id }}">
                <td><a href="{{ route('backend.authors.edit', $author->id) }}">{{ $author->name }}</a></td>
                <td>{{ $author->url }}</td>
                <td>{{ $author->channel_id }}</td>
                <td class="operations"><a href="#" data-action="delete"><i class="fa fa-window-close" aria-hidden="true"></i></a></td>
              </tr>
            @empty
              <tr>
                <td colspan="3">Авторы не добавлены</td>
              </tr>
            @endforelse
            </tbody>
          </table>
          {{ $authors->links() }}
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
  <script>
    $(function() {
        $('[data-action=delete]').click(function() {
           var id = $(this).closest('[data-id]').data('id');
           $.post('{{ route('backend.authors.index') }}/' + id, { _method: 'DELETE' }, function(data) {
               window.location.reload(true);
           });
           return false;
        });
    });
  </script>
@endpush
