@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Импорт автора'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <form method="POST" action="{{ route('backend.authors.import') }}">
          {{ csrf_field() }}
          <div class="form-group">
            <label for="channel_id">Id канала</label>
            <input type="text" class="form-control" name="channel_id" id="channel_id" value="{{ old('channel_id') }}">
          </div>
          <button type="submit" name="save" value="1" class="btn btn-default">Создать</button>
          <button type="submit" name="more" value="1" class="btn btn-default">Создать и добавить еще</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
