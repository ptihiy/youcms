@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Статьи'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.articles.create') }}" class="btn btn-md btn-primary">Создать статью</a>
      <div class="panel-body">
      </div>
    </div>
  </div>
</div>
@endsection
