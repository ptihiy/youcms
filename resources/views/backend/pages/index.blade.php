@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => 'Страницы'])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.pages.create') }}" class="btn btn-md btn-primary">Создать страницу</a>
      <div class="panel-body">
      </div>
    </div>
  </div>
</div>
@endsection
