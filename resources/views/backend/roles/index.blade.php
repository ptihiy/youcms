@extends('backend.master')

@section('content')
<div class="row">
  <div class="col-lg-12">
    <h1 class="page-header">Видео</h1>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.roles.create') }}" class="btn btn-md btn-primary">Создать роль</a>
      <div class="panel-body">
        <div class="canvas-wrapper">
        </div>
      </div>
    </div>
  </div>
</div><!--/.row-->
@endsection
