<script src="/backend/js/jquery-1.11.1.min.js"></script>
<script src="/backend/js/bootstrap.min.js"></script>
<script src="/backend/js/chart.min.js"></script>
<script src="/backend/js/chart-data.js"></script>
<script src="/backend/js/easypiechart.js"></script>
<script src="/backend/js/easypiechart-data.js"></script>
<script src="/backend/js/bootstrap-datepicker.js"></script>
<script src="/backend/js/custom.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.with-children').click(function() {
        $(this).toggleClass('active');
    });

    $('[data-transliterable]').on('keyup', function() {
        var $this = $(this);
        var target = $this.data('transliterable');
        var base = $this.data('base');
        var text = base ? base + '/' + $this.val() : $this.val();
        var translit = function(text) {
            var str = text.toString();
            var ru = ("А|а|Б|б|В|в|Ґ|ґ|Г|г|Д|д|Е|е|Ё|ё|Є|є|Ж|ж|З|з|И|и|І|і|Ї|ї|Й|й|К|к|Л|л|М|м|Н|н|О|о|П|п|Р|р|С|с|Т|т|У|у|Ф|ф|Х|х|Ц|ц|Ч|ч|Ш|ш|Щ|щ|Ъ|ъ|Ы|ы|Ь|ь|Э|э|Ю|ю|Я|я| |_").split("|");
            var en = ("A|a|B|b|V|v|G|g|G|g|D|d|E|e|E|e|E|e|ZH|zh|Z|z|I|i|I|i|I|i|J|j|K|k|L|l|M|m|N|n|O|o|P|p|R|r|S|s|T|t|U|u|F|f|H|h|TS|ts|CH|ch|SH|sh|SCH|sch|'|'|Y|y|'|'|E|e|YU|yu|YA|ya|-|-").split("|");
            var res = '';
            for(var i = 0, l = str.length; i < l; i++)
            {
                var s = str.charAt(i), n = ru.indexOf(s);
                if(n >= 0) {
                    res += en[n];
                }
                else {
                    res += s;
                }
            }
            return res.toLocaleLowerCase();
        }
        $('#' + target).val(translit(text));
        return false;
    });
</script>
@stack('scripts')
