
  <div class="row">
    <ol class="breadcrumb">
      <li><a href="{{ route('backend.dashboard') }}">
        <em class="fa fa-home"></em>
      </a></li>
      @foreach ($crumbs as $crumbName => $crumbLink)
        @if (!empty($crumbLink))
        <li><a href="{{ $crumbLink }}">{{ $crumbName }}</a></li>
        @else
        <li class="active">{{ $crumbName }}</li>
        @endif
      @endforeach
    </ol>
  </div>
