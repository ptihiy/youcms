<ul class="nav menu">
  <li class="{{ $menuItem === 'dashboard' ? 'active' : '' }}"><a href="{{ route('backend.dashboard') }}"><em class="fa fa-dashboard">&nbsp;</em> Панель управления</a></li>
  <li class="{{ $menuItem === 'categories' ? 'active' : '' }}"><a href="{{ route('backend.categories.index') }}"><em class="fa fa-folder-open-o">&nbsp;</em> Категории</a></li>
  <li class="{{ $menuItem === 'videos' ? 'active' : '' }}"><a href="{{ route('backend.videos.index') }}"><em class="fa fa-film">&nbsp;</em> Видео</a></li>
  <li class="{{ $menuItem === 'authors' ? 'active' : '' }}"><a href="{{ route('backend.authors.index') }}"><em class="fa fa-address-card-o">&nbsp;</em> Авторы</a></li>
  <li class="{{ $menuItem === 'news' ? 'active' : '' }}"><a href="{{ route('backend.news.index') }}"><em class="fa fa-rss">&nbsp;</em> Новости</a></li>
  <li class="{{ $menuItem === 'articles' ? 'active' : '' }}"><a href="{{ route('backend.articles.index') }}"><em class="fa fa-newspaper-o">&nbsp;</em> Статьи</a></li>
  <li class="{{ $menuItem === 'pages' ? 'active' : '' }}"><a href="{{ route('backend.pages.index') }}"><em class="fa fa-file-text-o">&nbsp;</em> Страницы</a></li>
  <li class="{{ $menuItem === 'comments' ? 'active' : '' }}"><a href="{{ route('backend.comments.index') }}"><em class="fa fa-comment-o">&nbsp;</em> Комментарии</a></li>
  <li class="{{ $menuItem === 'settings' ? 'active' : '' }}"><a href="{{ route('backend.settings.index') }}"><em class="fa fa-cogs">&nbsp;</em> Настройки</a></li>
  <li><a href="/logout"><em class="fa fa-power-off">&nbsp;</em> Выход</a></li>
</ul>
