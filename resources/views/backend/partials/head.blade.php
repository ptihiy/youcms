<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Панель управления</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="/backend/css/bootstrap.min.css" rel="stylesheet">
<link href="/backend/css/font-awesome.min.css" rel="stylesheet">
<link href="/backend/css/datepicker3.css" rel="stylesheet">
<link href="/backend/css/app.css" rel="stylesheet">

<!--Custom Font-->
<link href="https://fonts.googleapis.com/css?family=Arsenal:400,700&amp;subset=cyrillic" rel="stylesheet">
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
