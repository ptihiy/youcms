<!DOCTYPE html>
<html>
<head>
	@include('backend.partials.head')
</head>
<body>
	@include('backend.partials.navbar')
	@include('backend.partials.sidebar')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		@include('backend.partials.crumbs')
		@include('backend.partials.messages')
		@yield('content')
		@include('backend.partials.credit')
	</div>	

@include('backend.partials.scripts')
</body>
</html>
