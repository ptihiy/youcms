@extends('backend.master')

@section('content')
  @include('backend.partials.h1', ['h1' => $news->total().' '.pluralize($news->total(), 'новость,новости,новостей')])

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <a href="{{ route('backend.news.create') }}" class="btn btn-md btn-primary">Создать новость</a>
      <div class="panel-body">
      </div>
    </div>
  </div>
</div>
@endsection
