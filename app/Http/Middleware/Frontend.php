<?php

namespace App\Http\Middleware;

use App\Author;
use App\Category;
use Closure;
use View;

class Frontend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Loading items for menu
        $menu = [];

        // Categories
        $categories = Category::take(10)
            ->get();
        $menu['categories'] = $categories;

        // Authors
        $authors = Author::take(10)
            ->get();
        $menu['authors'] = $authors;

        View::share(['menu' => $menu]);

        return $next($request);
    }
}
