<?php

namespace App\Http\Middleware;

use Closure;
use View;

class Backend
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if ($user && $user->hasRole('manager')) {
            View::share(['manager' => $user]);

            return $next($request);
        }
        return redirect('/login');
    }
}
