<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Author;
use View;
use Youtube;

class AuthorsController extends Controller
{
    public function __construct()
    {
        View::share(['menuItem' => 'authors']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crumbs = [
            'Авторы' => '',
        ];

        $authors = Author::paginate(10);

        return view('backend.authors.index', compact('crumbs', 'authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $crumbs = [
            'Авторы' => route('backend.authors.index'),
            'Создать' => '',
        ];

        return view('backend.authors.create', compact('crumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:authors|max:255',
            'channel_id' => 'required|unique:authors|max:255',
            'url' => 'required|unique:authors|max:255'
        ]);

        $author = new Author;
        $author->name = $request->input('name');
        $author->channel_id = $request->input('channel_id');
        $author->url = $request->input('url');
        $author->save();

        $request->session()->flash('message', "Автор <strong>$author->name</strong> добавлен.");
        if ($request->has('more')) {
            return redirect()->route('backend.authors.create');
        }
        return redirect()->route('backend.authors.index');
    }

    public function request()
    {
        $crumbs = [
            'Авторы' => route('backend.authors.index'),
            'Импорт' => '',
        ];

        return view('backend.authors.request', compact('crumbs'));
    }

    public function import(Request $request)
    {
        $request->validate([
            'channel_id' => 'required',
        ]);

        Youtube::setApiKey('AIzaSyA2aKJiEsSfAi3cblRmw-co7TJ95zTlQ_A');
        $rawChannel = Youtube::getChannelById($request->input('channel_id'));
        $author = new Author;
        $author->name = $rawChannel->snippet->title;
        $author->url = translit($rawChannel->snippet->title);
        $author->channel_id = $rawChannel->id;
        $author->channel_title = $rawChannel->snippet->title;
        $author->save();

        $request->session()->flash('message', "Автор <strong>$author->name</strong> добавлен.");
        if ($request->has('more')) {
            return redirect()->route('backend.authors.create');
        }
        return redirect()->route('backend.authors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crumbs = [
            'Авторы' => route('backend.authors.index'),
            'Редактировать' => '',
        ];

        $author = Author::findOrFail($id);

        return view('backend.authors.edit', compact('author', 'crumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:authors,id,' . $id . '|max:255',
            'channel_id' => 'required|unique:authors,id,' . $id . '|max:255',
            'url' => 'required|unique:authors,id,' . $id . '|max:255'
        ]);

        $author = Author::findOrFail($id);
        $author->name = $request->input('name');
        $author->channel_id = $request->input('channel_id');
        $author->url = $request->input('url');
        $author->save();

        $request->session()->flash('message', "Автор <strong>$author->name</strong> обновлен.");
        return redirect()->route('backend.authors.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Author::destroy($id);
        return ['result' => 'success'];
    }
}
