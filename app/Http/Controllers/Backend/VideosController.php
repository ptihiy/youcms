<?php

namespace App\Http\Controllers\Backend;

use Youtube;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\Category;
use App\Author;
use View;
use Storage;
use File;
use Image;

class VideosController extends Controller
{
  public function __construct()
  {
    View::share(['menuItem' => 'videos']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $crumbs = [
        'Видео' => '',
      ];

      $videos = Video::with('author')
        ->paginate(10);

      return view('backend.videos.index', compact('crumbs', 'videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $crumbs = [
        'Видео' => route('backend.videos.index'),
        'Создать' => '',
      ];

      $authors = Author::get();

      $categories = Category::get();

      return view('backend.videos.create', compact('crumbs', 'categories', 'authors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'url' => 'required|unique:videos|max:255',
            'link' => 'required',
        ]);

        $video = new Video;
        $video->name = $request->input('name');
        $video->url = $request->input('url');
        $video->author_id = $request->input('author_id');
        $video->category_id = $request->input('category_id');
        $video->link = $request->input('link');
        $video->save();

        $request->session()->flash('message', "Видео <strong>$video->name</strong> добавлено.");
        if ($request->has('more')) {
            return redirect()->route('backend.videos.create');
        }
        return redirect()->route('backend.videos.index');
    }

    public function request()
    {
        $crumbs = [
            'Видео' => route('backend.videos.index'),
            'Импорт' => '',
        ];

        return view('backend.videos.request', compact('crumbs'));
    }

    public function import(Request $request)
    {
        $request->validate([
            'link' => 'required',
        ]);

        Youtube::setApiKey('AIzaSyA2aKJiEsSfAi3cblRmw-co7TJ95zTlQ_A');
        $rawVideo = Youtube::getVideoInfo($request->input('link'));

        $video = new Video;
        $video->name = $rawVideo->snippet->title;
        $video->url = translit($rawVideo->snippet->title);
        $video->summary = $rawVideo->snippet->description;
        $video->duration = $rawVideo->contentDetails->duration;
        $video->link = $rawVideo->id;

        // Get channel by title. Create new author, if new
        $channelId = $rawVideo->snippet->channelId;
        if ($channel = Author::byChannelId($channelId)->first()) {
            $video->author_id = $channel->id;
        } else {
            $rawChannel = Youtube::getChannelById($channelId);
            $author = new Author;
            $author->name = $rawChannel->snippet->title;
            $author->url = translit($rawChannel->snippet->title);
            $author->channel_id = $rawChannel->id;
            $author->channel_title = $rawChannel->snippet->title;
            $author->save();
            $video->author_id = $author->id;
        }
        // TODO: category
        $video->category_id = 15;

        // Importing thumbnails
        Image::make($rawVideo->snippet->thumbnails->default->url)->save(public_path('images/videos/'.$video->url.'_default.jpg'));
        Image::make($rawVideo->snippet->thumbnails->medium->url)->save(public_path('images/videos/'.$video->url.'_medium.jpg'));
        Image::make($rawVideo->snippet->thumbnails->high->url)->save(public_path('images/videos/'.$video->url.'_high.jpg'));
        Image::make($rawVideo->snippet->thumbnails->standard->url)->save(public_path('images/videos/'.$video->url.'_standard.jpg'));

        $video->save();

        $request->session()->flash('message', "Видео <strong>$video->name</strong> добавлено.");
        if ($request->has('more')) {
            return redirect()->route('backend.videos.create');
        }
        return redirect()->route('backend.videos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crumbs = [
            'Видео' => route('backend.videos.index'),
            'Редактировать' => '',
        ];

        $video = Video::findOrFail($id);

        $authors = Author::get();

        $categories = Category::get();

        return view('backend.videos.edit', compact('crumbs', 'authors', 'categories', 'video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'url' => 'required|unique:videos,id,'. $id .'|max:255',
            'link' => 'required',
        ]);

        $video = Video::findOrFail($id);
        $video->name = $request->input('name');
        $video->url = $request->input('url');
        $video->author_id = $request->input('author_id');
        $video->category_id = $request->input('category_id');
        $video->link = $request->input('link');
        $video->save();

        $request->session()->flash('message', "Видео <strong>$video->name</strong> обновлено.");
        return redirect()->route('backend.videos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
