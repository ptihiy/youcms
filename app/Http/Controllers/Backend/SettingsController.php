<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Setting;
use View;

class SettingsController extends Controller
{

  public function __construct()
  {
    View::share(['menuItem' => 'settings']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $crumbs = [
          'Настройки' => '',
        ];

        $settings = Setting::paginate(10);

        return view('backend.settings.index', compact('crumbs', 'settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $crumbs = [
        'Настройки' => route('backend.settings.index'),
        'Создать' => '',
      ];

      return view('backend.settings.create', compact('crumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
          'name' => 'required|unique:settings',
          'value' => 'required',
        ]);

        $setting = new Setting;
        $setting->name = $request->input('name');
        $setting->value = $request->input('value');
        $setting->save();

        if ($request->has('more')) {
          redirect()->route('backend.settings.create');
        }
        redirect()->route('backend.settings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
