<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

use DOMDocument;

class DashboardController extends Controller
{
  public function __construct()
  {
    View::share(['menuItem' => 'dashboard']);
  }

    public function index()
    {
      $crumbs = [
        'Панель управления' => '',
      ];

      return view('backend.dashboard.index', compact('crumbs'));
    }

}
