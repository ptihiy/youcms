<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use View;

class CategoriesController extends Controller
{
  public function __construct()
  {
    View::share(['menuItem' => 'categories']);
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $crumbs = [
        'Категории' => '',
      ];

        $categories = Category::getTree();

      $count = Category::count();

      return view('backend.categories.index', compact('crumbs', 'categories', 'count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $crumbs = [
        'Категории' => route('backend.categories.index'),
        'Создать' => '',
      ];

        $categories = Category::getTree();

      return view('backend.categories.create', compact('crumbs', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'url' => 'required|unique:categories|max:255',
        ]);

        $category = new Category;
        $category->name = $request->input('name');
        $category->url = $request->input('url');
        $parent = Category::where('url', $request->input('parent_id'))
            ->first();
        $category->parent_id = $parent ? $parent->id : 0;
        $category->summary = $request->input('summary');
        $category->content = $request->input('content');
        $category->save();

        $request->session()->flash('message', "Категория <strong>$category->name</strong> добавлена.");
        if ($request->has('more')) {
            return redirect()->route('backend.categories.create');
        }
        return redirect()->route('backend.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $crumbs = [
            'Категории' => route('backend.categories.index'),
            'Редактировать' => '',
        ];

        $category = Category::findOrFail($id);

        $categories = Category::getTree();

        return view('backend.categories.edit', compact('crumbs', 'category', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|max:255',
            'url' => 'required|unique:categories,id,'.$id.'|max:255',
        ]);

        $category = Category::findOrFail($id);
        $category->name = $request->input('name');
        $category->url = $request->input('url');
        $parent = Category::where('url', $request->input('parent_id'))
            ->first();
        $category->parent_id = $parent ? $parent->id : 0;
        $category->summary = $request->input('summary');
        $category->content = $request->input('content');
        $category->save();

        $request->session()->flash('message', "Категория <strong>$category->name</strong> обновлена.");
        return redirect()->route('backend.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Category::destroyWithChildren($id);
    }

    public function move(Request $request)
    {
        if (!$request->ajax()) {
            abort('405');
        }

        $request->validate([
            'id' => 'integer',
            'parent_id' => 'integer',
            'ids' => 'array',
        ]);

        $id = $request->input('id');
        $parentId = $request->input('parent_id');
        $ids = collect($request->input('ids'));

        // Get item
        $category = Category::findOrFail($id);
        $category->parent_id = $parentId;
        $category->save();

        // Get all siblings and sort them
        $siblings = Category::where('parent_id', $parentId)
            ->get()
            ->keyBy('id');
        $position = 0;
        foreach ($ids as $item) {
            $siblings[$item]->position = $position++;
            $siblings[$item]->save();
        }
    }
}
