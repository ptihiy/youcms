<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;
use App\Category;
use App\Author;

class IndexController extends Controller
{

  public function index()
  {
      $lastVideos = Video::with('author')
          ->orderBy('created_at', 'DESC')
          ->take(4)
          ->get();

      $popularVideos = Video::with('author')
          ->orderBy('views', 'DESC')
          ->take(5)
          ->get();

      $categories = Category::where('parent_id', 0)
          ->orderBy('position', 'ASC')
          ->orderBy('name', 'ASC')
          ->take(10)
          ->get();

      $authors = Author::take(10)
          ->get();

      return view('frontend.index.index', compact('lastVideos', 'popularVideos', 'categories', 'authors'));
  }

}
