<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Video;

class VideosController extends Controller
{
    public function show($url)
    {
      $video = Video::where('url', $url)
        ->with('author')
        ->firstOrFail();

      $video->views = $video->views + 1;
      $video->save();

      return view('frontend.videos.show', compact('video'));
    }
}
