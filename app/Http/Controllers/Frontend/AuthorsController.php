<?php

namespace App\Http\Controllers\Frontend;

use App\Author;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorsController extends Controller
{
    public function show($url)
    {
        $author = Author::ByUrl($url)
            ->firstOrFail();

        $videos = Video::byAuthor($author->id)
            ->paginate(9);

        return view('frontend.authors.show', compact('author', 'videos'));
    }
}
