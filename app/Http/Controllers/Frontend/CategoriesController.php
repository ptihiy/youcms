<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Video;

class CategoriesController extends Controller
{
    public function show($url)
    {
        $category = Category::ByUrl($url)
            ->firstOrFail();

        $videos = Video::byCategory($category->id)
            ->paginate(9);

        return view('frontend.categories.show', compact('category', 'videos'));
    }
}
