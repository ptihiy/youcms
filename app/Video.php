<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function author()
    {
        return $this->belongsTo('App\Author');
    }

    public function scopeByCategory($query, $categoryId)
    {
        $query->where('category_id', $categoryId);
    }

    public function scopeByAuthor($query, $authorId)
    {
        $query->where('author_id', $authorId);
    }
}