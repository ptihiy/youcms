<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function scopeByUrl($query, $url)
    {
        $query->where('url', $url);
    }

    public function scopeByChannelId($query, $channelId)
    {
        $query->where('channel_id', $channelId);
    }
}
