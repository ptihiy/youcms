<?php

if (!function_exists('pluralize')) {

    /**
     * Pluralize the message
     *
     * @param int $number
     * @param string $variants
     * @return string
     */
    function pluralize($number, $variants) {
        $variants = explode(',', $variants);
        if ($number > 10 && $number < 20) {
            return $variants[2];
        } else {
            $remainder = $number % 10;
            if ($remainder === 1) {
                return $variants[0];
            } else if (in_array($remainder, [2, 3, 4])) {
                return $variants[1];
            } else {
                return $variants[2];
            }
        }
    }
}

if (!function_exists('price')) {

    /**
     * Decorate price
     *
     * @param int $number
     * @return string
     */
    function price($price, $showCents = false) {
        if (strpos('.', $price) !== -1) {
            $fullPrice = explode('.', $price);
            if (count($fullPrice) === 1) {
                $showCents = false;
            } else {
                $cents = $fullPrice[1];
            }
            $price = $fullPrice[0];
        }
        $parts = str_split(strrev($price), 3);
        if ($showCents) {
            return implode('’', array_reverse(array_map(function($p) { return strrev($p); }, $parts))).'.'.$cents;
        }
        return implode('’', array_reverse(array_map(function($p) { return strrev($p); }, $parts)));
    }
}

if (!function_exists('rusmonth')) {

    function rusmonth($mnum)
    {
        return explode(" ", "января февраля марта апреля мая июня июля августа сентября октября ноября декабря")[intval($mnum) - 1];
    }

}

if (!function_exists('tab')) {
    function tab($str, $repeat) {
        for ($i = $repeat; $i > 0; $i--) {
            $str = "&nbsp;&nbsp;&nbsp;&nbsp;" . $str;
        }
        return $str;
    }
}

if (!function_exists('str_camelback')) {
    function str_camelback($str) {
        $words = preg_split('/[\s-]/', $str);
        $res = "";
        foreach ($words as $i => $word) {
            $res .= $i === 0 ? $word : ucfirst($word);
        }
        return $res;
    }
}
