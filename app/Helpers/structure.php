<?php

if (!function_exists('tree')) {
    // $d - flat data
    // $r - root element id
    // $pk - parent key name
    // $ch - children name
    function tree($d, $r = 0, $pk = 'parent_id', $k = 'id', $ch = 'children')
    {
        if (empty($d)) {
            return [];
        }
        $t = [];
        foreach ($d as $i) {
            isset($t[$i[$pk]]) ?: $t[$i[$pk]] = [];
            isset($t[$i[$k]]) ?: $t[$i[$k]] = [];
            $t[$i[$pk]][] = array_merge($i, [$ch => &$t[$i[$k]]]);
        }
        return $t[$r];
    }
}

if (!function_exists('array_build_tree')) {

}
