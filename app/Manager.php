<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laratrust\Traits\LaratrustUserTrait;

class Manager extends Model
{
    use LaratrustUserTrait;
    //
}
