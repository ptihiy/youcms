<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function scopeByUrl($query, $url)
    {
        $query->where('url', $url);
    }

    // Get full categories tree
    public static function getTree()
    {
        $categories = self::select('id', 'parent_id', 'name', 'url', 'position')
            ->orderBy('position')
            ->get();

        if ($categories) {
            $categoriesArr = $categories->toArray();
            $tree = self::buildTree($categoriesArr);
        }

        return $tree;
    }

    public function children()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    public static function destroyWithChildren($id)
    {
        $category = Category::with('children')
            ->findOrFail($id);

        $category->children->each(function($item, $key) {
           self::destroyWithChildren($item->id);
        });

        $category->delete();
    }

    public static function buildTree(array &$elements, $parentId = 0, $level = 0)
    {
        $name = __FUNCTION__;
        $branch = [];
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $element['level'] = $level;
                $children = self::$name($elements, $element['id'], $level + 1);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['id']] = $element;
            }
        }

        return $branch;
    }

    public static function positionAfterLast($parentId)
    {
        $maxAdjacentCategoriesPosition = Category::where('parent_id', $parentId)->pluck('position')->max();

        return $maxAdjacentCategoriesPosition + 1;
    }
}
